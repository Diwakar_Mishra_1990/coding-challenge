package Main.Utils.constants;
/**
 * These are the only two commands supported . Either I can add a child or i can
 * get a relationship
 * 
 * @author i350117
 *
 */
public enum Command {
	GET_RELATIONSHIP, ADD_CHILD
}
