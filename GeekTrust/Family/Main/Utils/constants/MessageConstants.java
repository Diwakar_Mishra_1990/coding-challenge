package Main.Utils.constants;
/**
 * contains message and error
 * @author i350117
 *
 */
public class MessageConstants {
	public static final String TYPE_ERROR = "ERROR";
	public static final String WRONG_COMMAND_PATTERN_AT_LINE = "WRONG COMMAND PATTERN AT LINE-";
	public static final String IS = " is ";
	public static final String OPERATION_NOT_SUPPORTED_REASON = "OPERATION NOT SUPPORTED AS ";
	public static final String CHILD_ADDITION_FAILED = "CHILD_ADDITION_FAILED";
	public static final String PERSON_NOT_FOUND = "PERSON_NOT_FOUND";
	public static final String CHILD_ADDITION_SUCCEEDED = "CHILD_ADDITION_SUCCEEDED";
	public static final String NONE = "NONE";
	public static final String COMA = ", ";
	public static final String ERRORS_WERE_DETECTED_WHILE_CREATING_THE_DICTIONARY = "ERRORS_WERE_DETECTED_WHILE_CREATING_THE_DICTIONARY";

}
