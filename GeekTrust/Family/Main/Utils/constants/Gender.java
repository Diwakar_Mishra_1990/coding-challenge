
package Main.Utils.constants;

/**
 * ENUM for Gender All the Member would have gender value within enum
 * 
 * @author i350117
 *
 */
public enum Gender {
	MALE, FEMALE;
}
