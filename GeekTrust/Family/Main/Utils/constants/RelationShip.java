package Main.Utils.constants;

/**
 * ENUM for Relationship .Following are the relationship supported as part of
 * our code. We can only give relationship based on below enum values
 * 
 * @author i350117
 *
 */
public enum RelationShip {
	FATHER, MOTHER, CHILDREN, SPOUSE, PATERNAL_UNCLE, PATERNAL_AUNT, MATERNAL_UNCLE, MATERNAL_AUNT, SISTER_IN_LAW, BROTHER_IN_LAW, SON, DAUGHTER, SIBLINGS;
}