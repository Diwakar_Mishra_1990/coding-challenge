package Main.Utils;

/**
 * contains all the utility method
 * @author i350117
 *
 */
public class Utils {
	/**
	 * Highlights message in a console
	 * 
	 * @param string
	 * @param string2
	 * @return
	 */
	public static void highlight(String message, String type) {
		if (message != null && message.length() > 0) {
			if (type != null && type.length() > 0) {
				System.out.println("##################################### - " + type.toUpperCase()
						+ " - ##########################################");
			} else {
				System.out.println("################################################################################");
			}
			System.out.println(message);
			System.out.println("################################################################################");
		}
	}
}
