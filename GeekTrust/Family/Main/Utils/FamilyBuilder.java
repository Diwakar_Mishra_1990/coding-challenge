package Main.Utils;

import java.util.HashMap;
import java.util.Map;

import Main.Beans.Member;
import Main.Utils.constants.Gender;

/**
 * Utility function to create family tree
 * 
 * @author i350117
 *
 */
public class FamilyBuilder {
	/**
	 * establishes parent child relationship between two mwmbers Takes Map which
	 * contain reference to object with key being name of member and value being the
	 * object itself as reference.
	 * 
	 * @param parentChildMap
	 * @param memberMap
	 * @throws Exception
	 */
	public static void buildParentChildRelationship(Map<String, String[]> parentChildMap, Map<String, Member> memberMap)
			throws Exception {
		for (String name : parentChildMap.keySet()) {
			for (int i = 0; i < parentChildMap.get(name).length; i++) {
				memberMap.get(name).addChildren(memberMap.get(parentChildMap.get(name)[i]));
			}
		}
	}

	/**
	 * Takes input a map with name and gender and returns a map with key being name
	 * and value being the reference to Member
	 * 
	 * @param inputMemberMap
	 * @return
	 */
	public static Map<String, Member> buildMemberMap(Map<String, Gender> inputMemberMap) {
		Map<String, Member> outputMemberMap = new HashMap<String, Member>();
		for (String name : inputMemberMap.keySet()) {
			Member newMember = new Member(inputMemberMap.get(name), name);
			outputMemberMap.put(name, newMember);
		}
		return outputMemberMap;
	}

	/**
	 * establishes husband wife relationship between to member Takes Map which
	 * contain reference to object with key being name of member and value being the
	 * object itself as reference.
	 * 
	 * @param husbandWifeMap
	 * @param memberMap
	 * @throws Exception
	 */
	public static void buildHusbandWifeRelationship(Map<String, String> husbandWifeMap, Map<String, Member> memberMap)
			throws Exception {
		for (String name : husbandWifeMap.keySet()) {
			memberMap.get(name).setSpouse((memberMap.get(husbandWifeMap.get(name))));
		}
	}

}
