package Main.Dictionary;
/**
 * We have assigned constants to the name of initial inputs. So that these constants can be used across
 * @author i350117
 *
 */
public class FamilyNameConstants {
	public static final String KRITHI = "KRITHI";
	public static final String KRIYA = "KRIYA";
	public static final String VASA = "VASA";
	public static final String LAVNYA = "LAVNYA";
	public static final String LAKI = "LAKI";
	public static final String YODHAN = "YODHAN";
	public static final String VYAS = "VYAS";
	public static final String ASVA = "ASVA";
	public static final String ATYA = "ATYA";
	public static final String KRPI = "KRPI";
	public static final String SATVY = "SATVY";
	public static final String AHIT = "AHIT";
	public static final String ARIT = "ARIT";
	public static final String JNKI = "JNKI";
	public static final String CHIKA = "CHIKA";
	public static final String VILA = "VILA";
	public static final String VRITHA = "VRITHA";
	public static final String JAYA = "JAYA";
	public static final String TRITHA = "TRITHA";
	public static final String DRITHA = "DRITHA";
	public static final String VYAN = "VYAN";
	public static final String ARAS = "ARAS";
	public static final String VICH = "VICH";
	public static final String ISH = "ISH";
	public static final String CHIT = "CHIT";
	public static final String SATYA = "SATYA";
	public static final String CHITRA = "CHITRA";
	public static final String LIKA = "LIKA";
	public static final String AMBA = "AMBA";
	public static final String ANGA = "ANGA";
	public static final String SHAN = "SHAN";
}
