package Main.Dictionary;

import java.util.HashMap;
import java.util.Map;

import Main.Beans.Member;
import Main.Utils.FamilyBuilder;
import Main.Utils.constants.Gender;
/**
 * This class as a whole can be termed as a class responsible for generating inputs for our program.
 * @author i350117
 *
 */
public class FamilyDictionary {
	//declaring root of dictionary
	public static final String root = FamilyNameConstants.SHAN;
	
	/**
	 * initializes family tree based on input
	 * also look at {@link FamilyBuilder} , which is used by this method to create relationship and creating members
	 * @return
	 * @throws Exception
	 */
	public static Member initialize() throws Exception{
		// This in a way also mimics the life cycle of human being
		// BIRTH->Marriage ->Children
		//Create gender map of input
		Map<String,Gender> genderMap = createGenderDictionary();
		//based on gender map create member map
		Map<String,Member> memberMap = FamilyBuilder.buildMemberMap(genderMap);
		//create husband wife relationship map
		Map<String,String> husbandWifeMap = createWifeHusband();
		//Build husband wife relationship
		FamilyBuilder.buildHusbandWifeRelationship(husbandWifeMap, memberMap);
		//create parent child map of input
		Map<String, String[]> parentChildMap = createMotherChildRelationship();
		//Build parent child relationship
		FamilyBuilder.buildParentChildRelationship(parentChildMap,memberMap);
	
		return memberMap.get(root);
	}
	/**
	 * creates map of member and there gender
	 * @return
	 */
	private static Map<String, Gender> createGenderDictionary() {
		Map<String, Gender> genderMap = new HashMap<String,Gender>();
		genderMap.put(FamilyNameConstants.SHAN, Gender.MALE);
		genderMap.put(FamilyNameConstants.ANGA, Gender.FEMALE);
		genderMap.put(FamilyNameConstants.AMBA, Gender.FEMALE);
		genderMap.put(FamilyNameConstants.LIKA, Gender.FEMALE);
		genderMap.put(FamilyNameConstants.CHITRA, Gender.FEMALE);
		genderMap.put(FamilyNameConstants.SATYA, Gender.FEMALE);
		genderMap.put(FamilyNameConstants.CHIT, Gender.MALE);
		genderMap.put(FamilyNameConstants.ISH, Gender.MALE);
		genderMap.put(FamilyNameConstants.VICH, Gender.MALE);
		genderMap.put(FamilyNameConstants.ARAS, Gender.MALE);
		genderMap.put(FamilyNameConstants.VYAN, Gender.MALE);
		genderMap.put(FamilyNameConstants.DRITHA, Gender.FEMALE);
		genderMap.put(FamilyNameConstants.TRITHA, Gender.FEMALE);
		genderMap.put(FamilyNameConstants.JAYA, Gender.MALE);
		genderMap.put(FamilyNameConstants.VRITHA, Gender.MALE);
		genderMap.put(FamilyNameConstants.VILA, Gender.FEMALE);
		genderMap.put(FamilyNameConstants.CHIKA, Gender.FEMALE);
		genderMap.put(FamilyNameConstants.JNKI, Gender.FEMALE);
		genderMap.put(FamilyNameConstants.ARIT, Gender.MALE);
		genderMap.put(FamilyNameConstants.AHIT, Gender.MALE);
		genderMap.put(FamilyNameConstants.SATVY, Gender.FEMALE);
		genderMap.put(FamilyNameConstants.KRPI, Gender.FEMALE);
		genderMap.put(FamilyNameConstants.ATYA, Gender.FEMALE);
		genderMap.put(FamilyNameConstants.ASVA, Gender.MALE);
		genderMap.put(FamilyNameConstants.VYAS, Gender.MALE);
		genderMap.put(FamilyNameConstants.YODHAN, Gender.MALE);
		genderMap.put(FamilyNameConstants.LAKI, Gender.MALE);
		genderMap.put(FamilyNameConstants.LAVNYA, Gender.FEMALE);
		genderMap.put(FamilyNameConstants.VASA, Gender.MALE);
		genderMap.put(FamilyNameConstants.KRIYA, Gender.MALE);
		genderMap.put(FamilyNameConstants.KRITHI, Gender.FEMALE);
		return genderMap;
	}
	/**
	 * create mother children relationship map.  It doesn't enforces any thing but a wrong value will cause
	 * error during the course of application
	 * @return
	 */
	private static Map<String, String[]> createMotherChildRelationship() {
		Map<String,String[]> parentChildList = new HashMap<String,String[]>();
		parentChildList.put(FamilyNameConstants.ANGA,new String[] {FamilyNameConstants.CHIT,FamilyNameConstants.ISH,FamilyNameConstants.VICH,FamilyNameConstants.ARAS,FamilyNameConstants.SATYA});
		parentChildList.put(FamilyNameConstants.AMBA,new String[] {FamilyNameConstants.DRITHA,FamilyNameConstants.TRITHA,FamilyNameConstants.VRITHA});
		parentChildList.put(FamilyNameConstants.LIKA,new String[] {FamilyNameConstants.VILA,FamilyNameConstants.CHIKA});
		parentChildList.put(FamilyNameConstants.CHITRA,new String[] {FamilyNameConstants.JNKI,FamilyNameConstants.AHIT});
		parentChildList.put(FamilyNameConstants.SATYA,new String[] {FamilyNameConstants.ASVA,FamilyNameConstants.VYAS,FamilyNameConstants.ATYA});
		parentChildList.put(FamilyNameConstants.DRITHA,new String[] {FamilyNameConstants.YODHAN});
		parentChildList.put(FamilyNameConstants.JNKI,new String[] {FamilyNameConstants.LAKI,FamilyNameConstants.LAVNYA});
		parentChildList.put(FamilyNameConstants.SATVY,new String[] {FamilyNameConstants.VASA});
		parentChildList.put(FamilyNameConstants.KRPI,new String[] {FamilyNameConstants.KRIYA,FamilyNameConstants.KRITHI});
		return parentChildList;
	}
	
	/**
	 * create husband wife relationship map
	 * key should be wife and value should be husband . It doesn't enforces any thing but a wrong value will cause
	 * error during the course of application
	 * @return
	 */
	private static Map<String, String> createWifeHusband() {
		Map<String,String> wifeHusband = new HashMap<String,String>();
		wifeHusband.put(FamilyNameConstants.ANGA,FamilyNameConstants.SHAN);
		wifeHusband.put(FamilyNameConstants.AMBA,FamilyNameConstants.CHIT);
		wifeHusband.put(FamilyNameConstants.LIKA,FamilyNameConstants.VICH);
		wifeHusband.put(FamilyNameConstants.CHITRA,FamilyNameConstants.ARAS);
		wifeHusband.put(FamilyNameConstants.SATYA,FamilyNameConstants.VYAN);
		wifeHusband.put(FamilyNameConstants.DRITHA,FamilyNameConstants.JAYA);
		wifeHusband.put(FamilyNameConstants.JNKI,FamilyNameConstants.ARIT);
		wifeHusband.put(FamilyNameConstants.SATVY,FamilyNameConstants.ASVA);
		wifeHusband.put(FamilyNameConstants.KRPI,FamilyNameConstants.VYAS);
		return wifeHusband;
	}

}
