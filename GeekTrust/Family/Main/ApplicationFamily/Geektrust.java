package Main.ApplicationFamily;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import Main.Beans.Member;
import Main.Dictionary.FamilyDictionary;
import Main.Helper.command.CommandHelper;
import Main.Utils.Utils;
import Main.Utils.constants.MessageConstants;

public class Geektrust {

	private static Map<String, Member> memberCache = new HashMap<String, Member>();
	private static Member family = null;

	public static void main(String[] args) {
		try {
			/**
			 * Initialize family tree with given data
			 */
			family = FamilyDictionary.initialize();
		} catch (Exception e) {
			System.out.println(MessageConstants.ERRORS_WERE_DETECTED_WHILE_CREATING_THE_DICTIONARY);
		}
		readInputCommandFile(args, family);
	}

	/**
	 * reads and process input from command line arguments
	 * 
	 * @param args
	 * @param family
	 */

	private static void readInputCommandFile(String[] args, Member family) {
		int index = 0;
		if (args!=null && args.length>0 ) {
			File file = new File(args[0]);
			// Read command from the files whose absolute path is provided in args[0]
			try (BufferedReader br = new BufferedReader(new FileReader(file))) {
				String line;
				while ((line = br.readLine()) != null) {
					index++;
					processInputLine(family, index, line);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * processes each input command line
	 * @param family
	 * @param index
	 * @param line
	 */
	private static void processInputLine(Member family, int index, String line) {
		// sanitize command
		line = CommandHelper.sanitizeCommand(line);
		// As attempt is to make a fail safe system , we ignore a command whose syntax
		// is not proper
		if (line != null) {
			performAction(family, line);
		} else {
			// show error in console
			showErrorInconsole(index);
		}
	}

	/**
	 * throws error in console
	 * 
	 * @param index
	 */
	private static void showErrorInconsole(int index) {
		// As attempt is to make a fail safe system , we ignore a command whose syntax
		// is not proper. However we
		// let the user know that command in which line has an issue. considering the
		// fact that our file may contain around
		// 1000 lines of command
		Utils.highlight(MessageConstants.WRONG_COMMAND_PATTERN_AT_LINE + index, MessageConstants.TYPE_ERROR);
	}

	/**
	 * performs the command which is represented by variable line
	 * 
	 * @param family
	 * @param line
	 */
	private static void performAction(Member family, String line) {
		// based on spaces we split command and input variables
		String[] cmdArray = line.split(" ");
		/**
		 * Its very important to translate command , because since input is a text file
		 * and the aim is to design a fail safe system , slight deviations like a
		 * different case of commands has been handled ie.GET_RELATIONSHIP and
		 * GeT_ReLaTiOnShIp would give same result similar is the case for add command
		 * and we wont fail a command for improper case
		 */
		CommandHelper.translateAndProcessCommand(family, cmdArray, memberCache);
	}

}
