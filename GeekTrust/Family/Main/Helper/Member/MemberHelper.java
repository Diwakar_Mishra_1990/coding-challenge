package Main.Helper.Member;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import Main.Beans.Member;
import Main.Utils.constants.Gender;
import Main.Utils.constants.MessageConstants;

/**
 * 
 * @author i350117
 *
 */
public class MemberHelper {


	/**
	 * Prints sister in law of member
	 * 
	 * @param member
	 * @param memberslist
	 */
	public static void printSisterInLaw(Member member) {
		List<Member> memberslist = new ArrayList<Member>();
		if (member.getFather() != null) {
			memberslist.addAll(member.getFather().getChildren().stream()
					.filter(n -> n.getspouse() != null && n.getspouse().getSex().equals(Gender.FEMALE) && n != member)
					.map(n -> n.getspouse()).collect(Collectors.toList()));
		}
		if (member.getspouse() != null) {
			memberslist.addAll(member.getspouse().getFather().getChildren().stream()
					.filter(n -> n.getSex().equals(Gender.FEMALE) && n != member).collect(Collectors.toList()));

		}
		if (memberslist != null && memberslist.size() == 0) {
			System.out.println(MessageConstants.NONE);
		} else {
			memberslist.stream().forEach(n -> {
				System.out.print(n + MessageConstants.COMA);
			});
			System.out.println();
		}
	}

	/**
	 * Print brother in law MessageConstants.MessageConstants.NONEof member
	 * 
	 * @param member
	 * @param memberslist
	 */
	public static void printBrotherinLaw(Member member) {
		List<Member> memberslist = new ArrayList<Member>();
		if (member.getFather() != null) {
			memberslist.addAll(member.getFather().getChildren().stream()
					.filter(n -> n.getspouse() != null && n.getspouse().getSex().equals(Gender.MALE) && n != member)
					.map(n -> n.getspouse()).collect(Collectors.toList()));
		}
		if (member.getspouse() != null) {
			memberslist.addAll(member.getspouse().getFather().getChildren().stream()
					.filter(n -> n.getSex().equals(Gender.MALE) && n != member).collect(Collectors.toList()));

		}
		if (memberslist != null && memberslist.size() == 0) {
			System.out.println(MessageConstants.NONE);
		} else {
			memberslist.stream().forEach(n -> {
				System.out.print(n + MessageConstants.COMA);
			});
			System.out.println();
		}
	}

	/**
	 * print siblings of member
	 * 
	 * @param member
	 * @param memberslist
	 */
	public static void printSiblings(Member member) {
		List<Member> memberslist = new ArrayList<Member>();
		if (member.getFather() != null) {
			memberslist = member.getFather().getChildren();
		}
		if (memberslist != null && memberslist.size() == 0) {
			System.out.println(MessageConstants.NONE);
		} else {
			memberslist.stream().forEach(n -> {
				if (n.getSex().equals(Gender.FEMALE) && n != member) {
					System.out.print(n + MessageConstants.COMA);
				}
			});
			System.out.println();
		}
	}

	/**
	 * print daughter of member
	 * 
	 * @param member
	 */
	public static void printDaughter(Member member) {
		List<Member> memberslist;
		memberslist = member.getChildren();
		if (memberslist != null && memberslist.size() == 0) {
			System.out.println(MessageConstants.NONE);
		} else {
			memberslist.stream().forEach(n -> {
				if (n.getSex().equals(Gender.FEMALE)) {
					System.out.print(n + MessageConstants.COMA);
				}
			});
			System.out.println();
		}
	}

	/**
	 * print son of member
	 * 
	 * @param member
	 */
	public static void printSon(Member member) {
		List<Member> memberslist;
		memberslist = member.getChildren();
		if (memberslist != null && memberslist.size() == 0) {
			System.out.println(MessageConstants.NONE);
		} else {
			memberslist.stream().forEach(n -> {
				if (n.getSex().equals(Gender.MALE)) {
					System.out.print(n + MessageConstants.COMA);
				}
			});
			System.out.println();
		}
	}

	/**
	 * print maternal aunt of member
	 * 
	 * @param member
	 * @param memberslist
	 */
	public static void printMaternalAunt(Member member) {
		List<Member> memberslist = new ArrayList<Member>();
		if (member.getMother() != null && member.getMother().getFather() != null) {
			memberslist = member.getMother().getFather().getChildren();
		}
		if (memberslist != null && memberslist.size() == 0) {
			System.out.println(MessageConstants.NONE);
		} else {
			memberslist.stream().forEach(n -> {
				if (n.getSex().equals(Gender.FEMALE)) {
					System.out.print(n + MessageConstants.COMA);
				}
			});
			System.out.println();
		}
	}

	/**
	 * print maternal uncle of member
	 * 
	 * @param member
	 * @param memberslist
	 */
	public static void printMaternalUncle(Member member) {
		List<Member> memberslist = new ArrayList<Member>();
		if (member.getMother() != null && member.getMother().getFather() != null) {
			memberslist = member.getMother().getFather().getChildren();
		}
		if (memberslist != null && memberslist.size() == 0) {
			System.out.println(MessageConstants.NONE);
		} else {
			memberslist.stream().forEach(n -> {
				if (n.getSex().equals(Gender.MALE)) {
					System.out.print(n + MessageConstants.COMA);
				}
			});
			System.out.println();
		}
	}

	/**
	 * print paternal aunt of member
	 * 
	 * @param member
	 * @param memberslist
	 */
	public static void printPaternalAunt(Member member) {
		List<Member> memberslist = new ArrayList<Member>();
		if (member.getFather() != null && member.getFather().getFather() != null) {
			memberslist = member.getFather().getFather().getChildren();
		}
		if (memberslist != null && memberslist.size() == 0) {
			System.out.println(MessageConstants.NONE);
		} else {
			memberslist.stream().forEach(n -> {
				if (n.getSex().equals(Gender.FEMALE)) {
					System.out.print(n + MessageConstants.COMA);
				}
			});
			System.out.println();
		}
	}

	/**
	 * print paternal uncle of member
	 * 
	 * @param member
	 * @param memberslist
	 */
	public static void printPaternalUncle(Member member) {
		List<Member> memberslist = new ArrayList<Member>();
		if (member.getFather() != null && member.getFather().getFather() != null) {
			memberslist = member.getFather().getFather().getChildren();
		}
		if (memberslist != null && memberslist.size() == 0) {
			System.out.println(MessageConstants.NONE);
		} else {
			memberslist.stream().forEach(n -> {
				if (n.getSex().equals(Gender.MALE)) {
					System.out.print(n + MessageConstants.COMA);
				}
			});
			System.out.println();
		}
	}

	/**
	 * print spouse of member
	 * 
	 * @param member
	 */
	public static void printSpouse(Member member) {
		if (member.getspouse() == null) {
			System.out.println(MessageConstants.NONE);
		} else {
			System.out.println(member.getspouse());
		}
	}

	/**
	 * print children of member
	 * 
	 * @param member
	 */
	public static void printChildren(Member member) {
		if (member.getChildren() == null && member.getChildren().size() > 0) {
			System.out.println(MessageConstants.NONE);
		} else {
			System.out.println(member.getChildren());
		}
	}

	/**
	 * print mother of member
	 * 
	 * @param member
	 */
	public static void printMother(Member member) {
		if (member.getMother() == null) {
			System.out.println(MessageConstants.NONE);
		} else {
			System.out.println(member.getMother());
		}
	}

	/**
	 * print father of member
	 * 
	 * @param member
	 */
	public static void printFather(Member member) {
		if (member.getFather() == null) {
			System.out.println(MessageConstants.NONE);
		} else {
			System.out.println(member.getFather());
		}
	}


}
