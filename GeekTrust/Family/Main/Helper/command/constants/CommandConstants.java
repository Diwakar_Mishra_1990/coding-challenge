package Main.Helper.command.constants;
/**
 * holds command related constants
 * @author i350117
 *
 */
public class CommandConstants {
	public static final String ADD_CHILD = "ADD_CHILD";
	public static final String GET_RELATIONSHIP = "GET_RELATIONSHIP";
	public static final String MALE = "MALE";
}
