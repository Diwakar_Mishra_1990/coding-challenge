package Main.Helper.command;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import Main.Beans.Member;
import Main.Helper.Relationship.RelationshipHelper;
import Main.Helper.command.constants.CommandConstants;
import Main.Utils.constants.Command;
import Main.Utils.constants.Gender;
import Main.Utils.constants.MessageConstants;
import Main.Utils.constants.RelationShip;

public class CommandHelper {

	/**
	 * removes extra spaces between commands checks for syntax and correctness of
	 * the command
	 * 
	 * @param command
	 * @return
	 */
	public static String sanitizeCommand(String command) {
		// Remove extra space in command. Again the system is designed for being fail
		// safe. and minor deviations in command has been taken care
		String commandWithProperSpacing = command.trim().replaceAll("\\s+", " ");
		String[] cmdArray = commandWithProperSpacing.split(" ");
		if (cmdArray != null) {
			// null is returned when no of strings in command is less than 3 and more than 4
			if (isValidcommandLength(cmdArray)) {
				return null;
			} else {
				return validateCommand(commandWithProperSpacing, cmdArray);
			}
		}
		// if all the conditions are not satisfied then we must return null
		return null;
	}

	/**
	 * checks for length of the command string , no of strings in command can be
	 * either three or four
	 * 
	 * @param cmdArray
	 * @return
	 */
	private static boolean isValidcommandLength(String[] cmdArray) {
		return cmdArray.length > 4 && cmdArray.length < 3;
	}

	/**
	 * once the command is valid we will return the string else we will put null
	 * 
	 * @param commandWithProperSpacing
	 * @param cmdArray
	 * @return
	 */
	private static String validateCommand(String commandWithProperSpacing, String[] cmdArray) {
		// if strings length in command is 3 the command must be GET_RELATIONSHIP
		if (validateGetRelationshipCommand(cmdArray)) {
			return null;
		}
		// if strings length in command is 4 the command must be ADD_CHILD
		else if (validateAddchildCommand(cmdArray)) {
			return null;
		} else {
			return validateCommandParameters(commandWithProperSpacing, cmdArray);
		}
	}

	/**
	 * validates add child command
	 * 
	 * @param cmdArray
	 * @return
	 */
	private static boolean validateAddchildCommand(String[] cmdArray) {
		return cmdArray.length == 4 && !cmdArray[0].equalsIgnoreCase(CommandConstants.ADD_CHILD);
	}

	/**
	 * validates get relationship command
	 * 
	 * @param cmdArray
	 * @return
	 */
	private static boolean validateGetRelationshipCommand(String[] cmdArray) {
		return cmdArray.length == 3 && !cmdArray[0].equalsIgnoreCase(CommandConstants.GET_RELATIONSHIP);
	}

	/**
	 * validates parameters of command
	 * 
	 * @param commandWithProperSpacing
	 * @param cmdArray
	 * @return
	 */
	private static String validateCommandParameters(String commandWithProperSpacing, String[] cmdArray) {
		// if command is ADD_CHILD last string must be of gender
		if (isAddChildParametersValid(cmdArray)) {
			return commandWithProperSpacing;
		}
		// if command is GET_RELATIONSHIP last string must be of relationship
		else if (iaGetRelationshipParameterValid(cmdArray)) {
			return commandWithProperSpacing;
		} else {
			// if all the conditions are not satisfied then we must return null
			return null;
		}
	}

	private static boolean iaGetRelationshipParameterValid(String[] cmdArray) {
		return cmdArray[0].equalsIgnoreCase(CommandConstants.GET_RELATIONSHIP)
				&& RelationshipHelper.getRelationshipType(cmdArray[2]) != null;
	}

	private static boolean isAddChildParametersValid(String[] cmdArray) {
		return cmdArray[0].equalsIgnoreCase(CommandConstants.ADD_CHILD)
				&& (cmdArray[3].trim().equalsIgnoreCase(Gender.FEMALE.toString())
						|| cmdArray[3].trim().equalsIgnoreCase(Gender.MALE.toString()));
	}

	/**
	 * Translates input into command. which can be executed in the family tree
	 * 
	 * @param family
	 * @param cmdString
	 */
	public static void translateAndProcessCommand(Member family, String[] cmdString, Map<String, Member> memberCache) {
		if (cmdString != null) {
			if (cmdString.length == 3) {
				processGetRelationshipCommand(family, cmdString);
			} else if (cmdString.length == 4) {
				// add children of member
				if (cmdString[0].equals(Command.ADD_CHILD.toString())) {
					processAddChildcommand(family, cmdString, memberCache);

				}
			}
		}
	}

	private static void processAddChildcommand(Member family, String[] cmdString, Map<String, Member> memberCache) {
		Member member = null;
		// first get value from membercache
		if (memberCache.containsKey(cmdString[1].toUpperCase())) {
			member = memberCache.get(cmdString[1].toUpperCase());
		}
		// else get value from family.find
		else {
			List<String> treePos = new ArrayList<String>();
			Member testMember = new Member(cmdString[1]);
			member = family.find(testMember, treePos);
			if (member != null) {
				performChildAddition(cmdString, memberCache, member);
			}
			// if member not found return error
			else {
				System.out.println(MessageConstants.PERSON_NOT_FOUND);
			}
		}
	}

	/**
	 * 
	 * @param cmdString
	 * @param memberCache
	 * @param member
	 */
	private static void performChildAddition(String[] cmdString, Map<String, Member> memberCache, Member member) {
		// if member is found add this in cache
		memberCache.put(cmdString[1].toUpperCase(), member);
		if (member.getSex() != Gender.FEMALE) {
			// If member is not female then return that child addition failed
			System.out.println(MessageConstants.CHILD_ADDITION_FAILED);
		} else {
			Member childMember = new Member(
					cmdString[3].equalsIgnoreCase(CommandConstants.MALE) ? Gender.MALE
							: Gender.FEMALE,
					cmdString[2].toUpperCase(), null, null);
			try {
				member.addChildren(childMember);
				System.out.println(MessageConstants.CHILD_ADDITION_SUCCEEDED);
				// once child added successfully add the child in cache for future reference
				memberCache.put(cmdString[2].toUpperCase(), childMember);
			} catch (Exception e) {
				// In case of any exception make sure child addition has failed
				System.out.println(MessageConstants.CHILD_ADDITION_FAILED);
			}

		}
	}

	/**
	 * process get relationship command
	 * @param family
	 * @param cmdString
	 */
	private static void processGetRelationshipCommand(Member family, String[] cmdString) {
		if (cmdString[0].equals(Command.GET_RELATIONSHIP.toString())) {
			String relationship = cmdString[2];
			RelationShip rel = RelationshipHelper.getRelationshipType(relationship);
			// If relationship is null then get the relationship of the member
			if (rel != null) {
				RelationshipHelper.getRelationShip(family, cmdString[1], rel);
			}
		}
	}
}
