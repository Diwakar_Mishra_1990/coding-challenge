package Main.Helper.Relationship;

import java.util.ArrayList;
import java.util.List;

import Main.Beans.Member;
import Main.Helper.Member.MemberHelper;
import Main.Helper.Relationship.constants.RelationShipConstants;
import Main.Utils.constants.RelationShip;

public class RelationshipHelper {


	/**
	 * maps input relationShip string to return RelationShip Enum
	 * 
	 * @param relationship
	 * @return
	 */
	public static RelationShip getRelationshipType(String relationship) {
		if (relationship.equalsIgnoreCase(RelationShipConstants.PATERNAL_UNCLE_TYPE1) || relationship.equalsIgnoreCase(RelationShipConstants.PATERNAL_UNCLE_TYPE2)) {
			return RelationShip.PATERNAL_UNCLE;
		} else if (relationship.equalsIgnoreCase(RelationShipConstants.PATERNAL_AUNT_TYPE1) || relationship.equalsIgnoreCase(RelationShipConstants.PATERNAL_AUNT_TYPE2)) {
			return RelationShip.PATERNAL_AUNT;
		} else if (relationship.equalsIgnoreCase(RelationShipConstants.MATERNAL_UNCLE_TYPE1) || relationship.equalsIgnoreCase(RelationShipConstants.MATERNAL_UNCLE_TYPE2)) {
			return RelationShip.MATERNAL_UNCLE;
		} else if (relationship.equalsIgnoreCase(RelationShipConstants.MATERNAL_AUNT_TYPE1) || relationship.equalsIgnoreCase(RelationShipConstants.MATERNAL_AUNT_TYPE_2)) {
			return RelationShip.MATERNAL_AUNT;
		} else if (relationship.equalsIgnoreCase(RelationShipConstants.SISTER_IN_LAW_TYPE1) || relationship.equalsIgnoreCase(RelationShipConstants.SISTER_IN_LAW_TYPE2)
				|| relationship.equalsIgnoreCase(RelationShipConstants.SISTER_IN_LAW_TYPE3) || relationship.equalsIgnoreCase(RelationShipConstants.SISTER_IN_LAW_TYPE4)) {
			return RelationShip.SISTER_IN_LAW;
		} else if (relationship.equalsIgnoreCase(RelationShipConstants.BROTHER_IN_LAW_TYPE1) || relationship.equalsIgnoreCase(RelationShipConstants.BROTHER_IN_LAW_TYPE2)
				|| relationship.equalsIgnoreCase(RelationShipConstants.BROTHER_IN_LAW_TYPE3) || relationship.equalsIgnoreCase(RelationShipConstants.BROTHER_IN_LAW_TYPE4)) {
			return RelationShip.BROTHER_IN_LAW;
		} else if (relationship.equalsIgnoreCase(RelationShipConstants.SIBLINGS) || relationship.equalsIgnoreCase(RelationShipConstants.SIBLING)) {
			return RelationShip.SIBLINGS;
		} else if (relationship.equalsIgnoreCase(RelationShipConstants.SON)) {
			return RelationShip.SON;
		} else if (relationship.equalsIgnoreCase(RelationShipConstants.DAUGHTER)) {
			return RelationShip.DAUGHTER;
		} else if (relationship.equalsIgnoreCase(RelationShipConstants.CHILDREN)) {
			return RelationShip.CHILDREN;
		} else if (relationship.equalsIgnoreCase(RelationShipConstants.SPOUSE)) {
			return RelationShip.SPOUSE;
		}
		return null;
	}

	public static void getRelationShip(Member family, String membername, RelationShip rel) {
		List<String> list = new ArrayList<String>();
		Member searchmember = new Member(membername);
		Member member = family.find(searchmember, list);
		switch (rel) {
		case FATHER:
			MemberHelper.printFather(member);
			break;
		case MOTHER:
			MemberHelper.printMother(member);
			break;
		case CHILDREN:
			MemberHelper.printChildren(member);
			break;
		case SPOUSE:
			MemberHelper.printSpouse(member);
			break;
		case PATERNAL_UNCLE:
			MemberHelper.printPaternalUncle(member);
			break;
		case PATERNAL_AUNT:
			MemberHelper.printPaternalAunt(member);
			break;
		case MATERNAL_UNCLE:
			MemberHelper.printMaternalUncle(member);
			break;
		case MATERNAL_AUNT:
			MemberHelper.printMaternalAunt(member);
			break;
		case SON:
			MemberHelper.printSon(member);
			break;
		case DAUGHTER:
			MemberHelper.printDaughter(member);
			break;
		case SIBLINGS:
			MemberHelper.printSiblings(member);
			break;
		case BROTHER_IN_LAW:
			MemberHelper.printBrotherinLaw(member);
			break;
		case SISTER_IN_LAW:
			MemberHelper.printSisterInLaw(member);
			break;
		}
	}

}
