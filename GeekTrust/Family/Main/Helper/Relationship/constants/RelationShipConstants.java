package Main.Helper.Relationship.constants;

public class RelationShipConstants {
	public static final String SPOUSE = "SPOUSE";
	public static final String CHILDREN = "children";
	public static final String DAUGHTER = "DAUGHTER";
	public static final String SON = "son";
	public static final String SIBLING = "sibling";
	public static final String SIBLINGS = "siblings";
	public static final String BROTHER_IN_LAW_TYPE4 = "Brother_In_Law";
	public static final String BROTHER_IN_LAW_TYPE3 = "Brother_In-Law";
	public static final String BROTHER_IN_LAW_TYPE2 = "Brother-In_Law";
	public static final String BROTHER_IN_LAW_TYPE1 = "Brother-In-Law";
	public static final String SISTER_IN_LAW_TYPE4 = "Sister_In_Law";
	public static final String SISTER_IN_LAW_TYPE3 = "Sister_In-Law";
	public static final String SISTER_IN_LAW_TYPE2 = "Sister-In_Law";
	public static final String SISTER_IN_LAW_TYPE1 = "Sister-In-Law";
	public static final String MATERNAL_AUNT_TYPE_2 = "Maternal_Aunt";
	public static final String MATERNAL_AUNT_TYPE1 = "Maternal-Aunt";
	public static final String MATERNAL_UNCLE_TYPE2 = "maternal_Uncle";
	public static final String MATERNAL_UNCLE_TYPE1 = "Maternal-Uncle";
	public static final String PATERNAL_AUNT_TYPE2 = "Paternal_Aunt";
	public static final String PATERNAL_AUNT_TYPE1 = "Paternal-Aunt";
	public static final String PATERNAL_UNCLE_TYPE2 = "Paternal_Uncle";
	public static final String PATERNAL_UNCLE_TYPE1 = "Paternal-Uncle";
}
