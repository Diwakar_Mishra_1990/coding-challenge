package Main.Beans;

import java.util.ArrayList;
import java.util.List;

import Main.Utils.constants.Gender;
import Main.Utils.constants.MessageConstants;

/**
 * this is the basic data structure used . It is basically a tree data structure
 * 
 * @author i350117
 *
 */
public class Member {

	private Gender sex;
	private String name;
	private Member father;
	private Member mother;
	private Member spouse;
	private List<Member> childrens;

	/**
	 * Initialize a member based on name
	 * 
	 * @param name
	 */
	public Member(String name) {
		this.name = name;
	}
	
	/**
	 * 
	 * @param sex
	 * @param name
	 */
	public Member(Gender sex, String name) {
		this.sex = sex;
		this.name = name;
		this.childrens = new ArrayList<Member>();
	}

	/**
	 * Initialize based on all the fields
	 * 
	 * @param sex
	 * @param name
	 * @param father
	 * @param mother
	 */
	public Member(Gender sex, String name, Member father, Member mother) {
		this.sex = sex;
		this.name = name;
		this.father = father;
		this.mother = mother;
		this.childrens = new ArrayList<Member>();
	}

	/**
	 * getter for gender
	 * 
	 * @return
	 */
	public Gender getSex() {
		return sex;
	}

	/**
	 * setter for gender
	 * 
	 * @param sex
	 */
	public void setSex(Gender sex) {
		this.sex = sex;
	}

	/**
	 * getter for name
	 * 
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * setter for name
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * getter for father
	 * 
	 * @return
	 */
	public Member getFather() {
		return father;
	}

	/**
	 * setter for father
	 * 
	 * @param father
	 */
	public void setFather(Member father) {
		this.father = father;
	}

	/**
	 * getter for mother
	 * 
	 * @return
	 */
	public Member getMother() {
		return mother;
	}

	/**
	 * setter for mother
	 * 
	 * @param mother
	 */
	public void setMother(Member mother) {
		this.mother = mother;
	}

	/**
	 * getter for children
	 * 
	 * @return
	 */
	public List<Member> getChildren() {
		return childrens;
	}

	/**
	 * setter for children
	 * 
	 * @param childrens
	 */
	public void setChildren(List<Member> childrens) {
		this.childrens = childrens;
		if (this.getSex().equals(Gender.FEMALE)) {
			this.getspouse().setChildren(childrens);
		}
	}

	/**
	 * add children to children list
	 * 
	 * @param children
	 * @throws Exception
	 */
	public void addChildren(Member children) throws Exception {
		// child addition only applicable for female members
		if (this.getSex() == Gender.FEMALE) {
			children.setFather(this.getspouse());
			children.setMother(this);
			if (!this.childrens.contains(children)) {
				this.childrens.add(children);
			}
			if (this.getspouse() != null) {
				// once added to mother add to spouse
				this.getspouse().addChildrenforMale(children);
			}
		}
		// if member is female direct addition not possible
		else {
			throw new Exception(MessageConstants.CHILD_ADDITION_FAILED);
		}
	}

	/**
	 * Only add childrens to male member
	 * 
	 * @param children
	 */
	private void addChildrenforMale(Member children) {
		children.setFather(this);
		if (!this.childrens.contains(children)) {
			this.childrens.add(children);
		}

	}

	/**
	 * getter for spouse
	 * 
	 * @return
	 */
	public Member getspouse() {
		return spouse;
	}

	/**
	 * setter for spouse
	 * 
	 * @param spouse
	 * @throws Exception
	 */
	public void setSpouse(Member spouse) throws Exception {
		this.spouse = spouse;
		// we can set spouse only for female
		if (this.getSex() == Gender.FEMALE) {
			spouse.setSpouseForMale(this);
			spouse.setChildren(this.childrens);
		} else {
			throw new Exception(MessageConstants.OPERATION_NOT_SUPPORTED_REASON + this.getName() + MessageConstants.IS + this.getSex().toString());
		}
	}

	/**
	 * set spouse only for male
	 * 
	 * @param spouse
	 * @throws Exception
	 */
	private void setSpouseForMale(Member spouse) throws Exception {
		if (this.getSex() == Gender.MALE) {
			this.spouse = spouse;
		} else {
			throw new Exception(MessageConstants.OPERATION_NOT_SUPPORTED_REASON + this.getName() + MessageConstants.IS + this.getSex().toString());
		}
	}

	/**
	 * find member in the family tree with member name
	 * 
	 * @param memberName
	 * @param list
	 * @return
	 */
	public Member find(Member memberName, List<String> list) {
		Member children;
		if (this.equals(memberName)) {
			list.add(this.getName());
			// System.out.println(list);
			return this;
		}
		// first find in children
		if (this.childrens != null || this.childrens.size() == 0) {
			for (int i = 0; i < this.childrens.size(); i++) {
				Member child = this.childrens.get(i);
				children = child.find(memberName, list);
				if (children == null) {
					list.clear();// = "";
				}
				if (children != null) {
					list.add(0, this.getName());
					// list = this.getName()+"/"+list;
					// System.out.println(list);
					return children;
				}
			}
		}
		// if not found in children find in spouse
		if (this.getspouse() != null && this.getspouse().equals(memberName)) {
			list.add(0, this.getspouse().getName());
			// list = this.getName()+"/"+list;
			// System.out.println(list);
			return this.getspouse();
		}
		return null;
	}

	/**
	 * returns hashcode
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	/**
	 * Since we enter child based on mothers name.Hence only name is used as
	 * parameter of comparison. Owing to the fact that there cannot be duplicate
	 * names with duplicate sex
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Member)) {
			return false;
		}
		Member other = (Member) obj;
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equalsIgnoreCase(other.name)) {
			return false;
		}
		return true;
	}

	/**
	 * To string method is overriden so that in list we are able to print object
	 * conveniently
	 */
	@Override
	public String toString() {
		return this.getName();
	}
}